# Variávies
CC = gcc
CFLAGS = -pedantic-errors -Wall

# Regra : dependências
all: buscaBinaria.c
	$(CC) $(CFLAGS) buscaBinaria.c -o buscaBinaria

clean:
	buscaBinaria

run:
	./buscaBinaria

debug:
	make CFLAGS+=-g
