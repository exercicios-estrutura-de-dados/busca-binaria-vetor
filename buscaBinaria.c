#include <stdio.h>
#define SIZE_V 100
/* 
    Exibir "prints" detalhando a busca 
    #define _detalhar_busca_ 1 // ativado
    #define _detalhar_busca_ 0 // desativado
*/
#define _detalhar_busca_ 0 // desativado

// Busca binária
int buscaBinaria(int numero, int vetor[], int tamanhoVetor){
    if(tamanhoVetor < 0) return -1; // Vetor de tamanho inválido

    int inicioVetor=0;
    int fimVetor=tamanhoVetor-1;

    while(inicioVetor <= fimVetor){
        int meioVetor = (inicioVetor+fimVetor)/2;   // Calcula posição do meio do vetor
        
        #if (_detalhar_busca_ == 1)
            printf(
                "\n------ buscando... -------\n"
                "Vetor: "
            );
            for(int i=inicioVetor; i<= fimVetor; i++) printf("%d ", vetor[i]);
            printf(
                "\n"
                "Valor no incio: %d\n"
                "Valor no meio: %d\n"
                "Valor no fim: %d\n",
                vetor[inicioVetor],
                vetor[meioVetor],
                vetor[fimVetor]
            );
        #endif

                                                    // [ esquerda/inicioVetor ] < [ meioVetor ] < [ direita/fimVetor ]
        if(numero ==  vetor[meioVetor]){            // → Número está no meio do vetor?
            return vetor[meioVetor];                //      então encontrou o número 
        }
        else if(numero < vetor[meioVetor]){         // → Número está à esquerda de "vetor[meio]"?
            fimVetor = meioVetor-1;                 //      então ignora a metade da direita
            
            #if (_detalhar_busca_ == 1)
                printf("%d está à esquerda de %d\n", numero, vetor[meioVetor]);
            #endif
        }
        else{                                       // → Se não, número está à direita de "vetor[meio]"
            inicioVetor=meioVetor+1;                //      então ignora a metade esquerda

            #if (_detalhar_busca_ == 1)
                printf("%d está à direita de %d\n", numero, vetor[meioVetor]);
            #endif
        }
    }
    return -1; // Número não encontrado
}

int main(){
    int i, j, numeroBuscado, resultado;
    //int vetor[SIZE_V] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100};
    int vetor[SIZE_V] = {5,19,26,35,47,55,61,72,73,80,81,83,85,87,90,93,100,116,131,137,144,154,175,185,198,201,202,203,204,206,215,241,245,246,274,281,299,322,330,348,350,357,379,380,385,403,406,413,416,418,422,430,441,446,456,459,463,465,497,514,549,553,578,581,582,587,588,590,593,595,602,605,611,621,630,634,637,638,651,664,668,669,685,686,697,721,730,735,739,744,751,762,763,766,767,769,789,790,796,800};
    printf(
        "\033[1;31m"
        "======================= Busca binária =======================\n\n\033[0m"
        "+-----------------------------------------------------------+\n"
        "|                           Vetor                           |\n"
        "+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+\n"
    );
    for(i=0; i<SIZE_V; i+=10){
        for(j=0; j<10; j++){
            printf(
                "| %3d ",
                vetor[i+j]
            );
        }
        printf("|\n+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+\n"
        );
    }
    printf("\nQual número deseja buscar? ");
    scanf("%d", &numeroBuscado);
    
    resultado = buscaBinaria(numeroBuscado, vetor, sizeof(vetor)/sizeof(int));
    if(resultado >= 0){
        printf("\nNúmero %d encontrado. \n", resultado);
    }else{
        printf("\nNúmero não encontrado. \n");
    }
    return 0;
}
